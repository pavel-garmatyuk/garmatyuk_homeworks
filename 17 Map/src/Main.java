import java.util.Iterator;
import java.util.Scanner;
import java.util.Map;
import java.util.HashMap;

public class Main {
    public static void main(String[] args) {
        System.out.println("Введите слова: ");
        String input = new Scanner(System.in).nextLine();
        String[] words = input.split("\\s+");

        Map<String, Integer> countEqualsWords = new HashMap<>();
        for (int i = 0; i < words.length; i++) {
            String s = words[i];
            if (!countEqualsWords.containsKey(s)) {
                countEqualsWords.put(s, 0);
            }
            countEqualsWords.put(s, countEqualsWords.get(s) + 1);
        }
        for (Iterator<String> iterator = countEqualsWords.keySet().iterator(); iterator.hasNext(); ) {
            String word = iterator.next();
            String s = word + " " + countEqualsWords.get(word) + " шт";
            System.out.println(s);
        }
    }
}
