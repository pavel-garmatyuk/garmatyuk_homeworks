package com.company;

public class Main {
    public static int minCount(int... array) {
        int[] buffer = new int[201];
        for (int i = 0; i < array.length - 1; i++) {
            if (array[i] >= 0) {
                buffer[array[i]]++;
            }
            if (array[i] < 0) {
                int index =  Math.abs(array[i]) + 100;
                buffer[index]++;
            }
        }
        int minValue = getMinValue(buffer);
        return minValue > 100 ? (minValue - 100) * -1 : minValue;
    }

    public static int getMinValue(int[] buffer) {
        int min = Integer.MAX_VALUE;
        int index = -1;
        for (int i = 0; i < buffer.length - 1; i++) {
            if (buffer[i] != 0) {
                if (min > buffer[i]) {
                    min = buffer[i];
                    index = i;
                }
            }
        }
        return index;
    }
}