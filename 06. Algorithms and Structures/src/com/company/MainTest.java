package com.company;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

class MainTest {

    @Test
    void minCount() {
        int[] array = {10, 20, 3, -99, 10, 3, 20, -99, -99, 11, -1};
        Assertions.assertEquals(Main.minCount(array), 11);
    }

    @Test
    void minCount_big_array() {
        int[] array = {10, 20, 3, 100, 100, -7, 10, 20, 3, 100, -7, 10, 20, 3, -99, -99, -7, 10, 20, 3, -99, -7, -2, -2};
        Assertions.assertEquals(Main.minCount(array), -2);
    }
}