public class SumThread extends Thread {
    private int from;
    private int to;


    public SumThread(int from, int to) {

        this.from = from;
        this.to = to;

    }
    @Override
    public void run () {
        Main.sum = 0;
        for (int i = this.from; i <= this.to; i++) {
            if(i < Main.array.length)
                Main.sum += Main.array[i];
        }
    }
}
