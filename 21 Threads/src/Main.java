import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;
import java.util.stream.IntStream;

public class Main {

    public static int array[];
    public static int sums[];
    public static int sum;

    public static void main(String[] args) {
        Random random = new Random();
        Scanner scanner = new Scanner(System.in);
        int numbersCount = scanner.nextInt();
        int threadsCount = scanner.nextInt();

        array = new int[numbersCount];
        sums = new int[threadsCount];

        IntStream.range(0, array.length).forEach(i -> array[i] = random.nextInt(100));

        long startTime = System.currentTimeMillis();

        int realSum = Arrays.stream(array).sum();

        long timeForCycle = System.currentTimeMillis() - startTime;

        System.out.println("Сумма элементов в цикле: " + realSum);
        System.out.println("Время выполнения: " + timeForCycle);

        // TODO: реализовать работу с потоками
        long startTimeThreads = System.currentTimeMillis();

        int byThreadSum = 0;
        int numberOfElementsInOneThread = (int)Math.ceil((double)(numbersCount) / (double)(threadsCount));
        int[] sums = new int[threadsCount];
        for (int i = 0; i < sums.length; i++) {
            SumThread SumThread = new SumThread(i * numberOfElementsInOneThread, (i + 1) * numberOfElementsInOneThread -1);
            SumThread.start();
            try {
                SumThread.join();
            } catch (InterruptedException e) {
                throw new IllegalArgumentException();
            }
            sums[i] = sum;
            byThreadSum += sums[i];
        }

        long timeForThreads = System.currentTimeMillis() - startTimeThreads;

        System.out.println("Сумма элементов массива в потоке: " + byThreadSum);
        System.out.println("Время выполнения: " + timeForThreads);
    }
}
