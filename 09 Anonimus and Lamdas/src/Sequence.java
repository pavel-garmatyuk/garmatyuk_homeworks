import java.util.Arrays;

public class Sequence {
    /**
     * @param array input array
     * @param condition логическое выражение в интерфейсе condition, определенное в методе isOk
     * @return массив, который содержит элементы, удовлетворяющиие логическому выражению в condition.
     */
    public static int[] filter(int[] array, ByCondition condition) {
        int[] tmpArray = new int[array.length];
        int count = 0;
        for (int i = 0; i < array.length; i++) {
            if (condition.isOk(array[i])) {
                tmpArray[count++] = array[i];
            }
        }
        return Arrays.copyOf(tmpArray, count);
    }
}
