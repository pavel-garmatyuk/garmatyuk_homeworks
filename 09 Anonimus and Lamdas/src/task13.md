###### Предусмотреть функциональный интерфейс

`interface ByCondition {
	boolean isOk(int number);
}`

###### Реализовать в классе Sequence метод:

`public static int[] filter(int[] array, ByCondition condition) {
	...
}
`

_Данный метод возвращает массив, который содержит элементы, удовлетворяющиие логическому выражению в condition._

###### В main в качестве condition подставить:

- проверку на четность элемента
- проверку, является ли сумма цифр элемента четным числом.