import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        //задать случайную длину массива
        int inputArrayLength = (int) (Math.random() * 100);
        System.out.println("Количество элементов массива равно: " + inputArrayLength);
        //зная длину массива, генерируем элементы массива
        int[] inputArray = new int[inputArrayLength];
        for (int i = 0; i < inputArray.length; i++) {
            inputArray[i] = (int) ((Math.random() * 271) + 100 / ((Math.random() * 26) + 100) - (Math.random() * 137) + 100 / ((Math.random() * 26) + 100));
        }
        System.out.println(Arrays.toString(inputArray));

        ByCondition condition;
        //получаем массив с четными элементами
        int[] evenElementsArray = new int[inputArray.length];
        int countEvenElementsArray = 0;

        for (int i = 0; i < inputArray.length; i++) {
            if (isEven(inputArray[i])) {
                evenElementsArray[countEvenElementsArray] = inputArray[i];
                countEvenElementsArray++;
            }
        }
        System.out.println("Получаем массив с четными элементами, равный: " + countEvenElementsArray);
        evenElementsArray = Arrays.copyOf(evenElementsArray, countEvenElementsArray);
        System.out.println(Arrays.toString(evenElementsArray));

        //Получаем массив с четной суммой цифр
        int countEvenSumDigits = 0;
        int[] evenSumDigits = new int[evenElementsArray.length];

        for (int i = 0; i < evenElementsArray.length; i++) {
            if (isSumElementsArray(evenElementsArray[i])) {
                evenSumDigits[countEvenSumDigits] = evenElementsArray[i];
                countEvenSumDigits++;
            }
        }
        System.out.println("Получаем массив с четной суммой цифр, равный: " + countEvenSumDigits);
        evenSumDigits = Arrays.copyOf(evenSumDigits, countEvenSumDigits);
        System.out.println(Arrays.toString(evenSumDigits));

    }

    public static boolean isEven(int input) {
        return (input % 2 == 0);
    }

    public static boolean isSumElementsArray(int input) {
        int sum = 0;
        while (input != 0) {
            sum += input % 10;
            input /= 10;
        }
        return isEven(sum);
    }
}