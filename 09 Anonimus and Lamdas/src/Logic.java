import java.util.Arrays;

public class Logic {
    public static void main(String[] args) {
        int[] inputArray = {78, 164, 84, 58, 36, 113, -34, 77, 65, 171, 83, 40, 196, -82, 149, 192, 13, -42, -82, 7, 45, -42, -88, 171, -63, 123, 40, 45};

        ByCondition conditionSumNumberElementArrayEven = number -> {
            int sumDigitalNumber = 0;
            while (number > 0) {
                sumDigitalNumber += number % 10;
                number /= 10;
            }
            return sumDigitalNumber % 2 == 0;
        };
        int[] evenArray = Sequence.filter(inputArray, number -> number % 2 == 0);
        System.out.println(Arrays.toString(evenArray));
        int[] evenDigitalNumberArray = Sequence.filter(inputArray, conditionSumNumberElementArrayEven);
        System.out.println(Arrays.toString(evenDigitalNumberArray));




    }

}
