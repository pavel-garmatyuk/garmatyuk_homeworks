package LinkedList;

public class LinkedList<T> {
    private static class Node<T> {
        T value;
        Node<T> next;

        public Node(T value) {
            this.value = value;
        }
    }

    private Node<T> first; //ссылка на первую Ноду
    private Node<T> last; // ссылка на крайнюю Ноду
    private int size;

    public void add(T element) {
        //создаю новый узел(новую Ноду)
        Node<T> newNode = new Node<>(element);
        if (size == 0) {
            first = newNode;
        } else {
            last.next = newNode;
        }
        last = newNode;
        size++;
    }

    public void addToBegin(T element) {
        Node<T> newNode = new Node<>(element);

        if (size == 0) {
            last = newNode;
        } else {
            newNode.next = first;
        }
        first = newNode;
        size++;
    }

    public int size() {
        return size;
    }

    public T get(int index) {
        Node<T> correctNode = first;
        if (isCorrectIndex(index)) {

            for (int i = 0; i < index; i++) {
                correctNode = correctNode.next;
            }
        } else
            return null;

        return (T) correctNode.value;
    }

    private boolean isCorrectIndex(int index) {
        return index >= 0 && index < size;
    }

    public int length() {
        return size;
    }
}
                                                                                                                             