package com.company;

public class Humans implements Comparable<Humans> {
    private String name;
    private float weight;

    public Humans() {
    }

    public Humans(String name) {
        this.name = name;
    }

    public Humans(String name, float weight) {
        this(name);
        this.weight = weight;
    }

    public float getWeight() {
        return weight;
    }

    @Override
    public String toString() {
        return String.format("Имя:%s,  Вес: %.2f", name, weight);
    }

    @Override
    public int compareTo(Humans o) {
        return (int) (this.weight - o.weight);
    }
}
