package com.company;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {

        Humans[] humans = new Humans[10];

        for (int i = 0; i < humans.length; i++) {
            humans[i] = new Humans("first_name_" + i, (float) ((Math.random() * 101) / 3.17) + 57);
        }

        //System.out.println(Arrays.toString(humans));

        //Некрасивый вариант
        //Arrays.sort(humans);
        //System.out.println(Arrays.toString(humans));

        //Красивый и изящный вариант
        //Arrays.stream(humans).forEach(System.out::println);

        humansWeightSort(humans);
        for (Humans human : humans) {
            System.out.println(human.toString());
        }
    }

    private static void humansWeightSort(Humans... humans) {

        for (int i = 0; i < humans.length - 1; i++) {
            int count = i;
            for (int j = i; j < humans.length; j++) {
                if (humans[j].getWeight() < humans[count].getWeight()) {
                    count = j;
                }
            }
            Humans tmp = humans[i];
            humans[i] = humans[count];
            humans[count] = tmp;
        }
    }
}
