import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        try (BufferedReader reader = new BufferedReader(new FileReader("input.txt"))) {
            reader.lines()
                    .map(line -> line.split("\\|"))
                    .filter((String[] lib) -> "Black".equalsIgnoreCase(lib[2]) || 0 == Integer.parseInt(lib[3]))
                    .forEach(lib -> System.out.println(lib[0]));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
