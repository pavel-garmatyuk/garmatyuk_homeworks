package theory.singleton;

public class Dictionary {
    private static final Dictionary instance;

    static {
        instance = new Dictionary();
    }

    private static final int MAX_WORD_COUNT = 100;
    private Dictionary() {
        this.words = new String[MAX_WORD_COUNT];
    }

    public static Dictionary getInstance() {
        return instance;
    }

    private String[] words;

    private int count;

    public void add(String word) {
        if (count < MAX_WORD_COUNT) {
            this.words[count] = word;
            count++;
        } else {
            System.err.println("Переполнение словаря.");
        }
    }

    public String complete(String word) {
        for (int i = 0; i < count; i++) {
            if (this.words[i].startsWith(word)) {
                return this.words[i];
            }
        }
        return "";
    }
}
