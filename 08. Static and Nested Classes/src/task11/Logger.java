package task11;

public class Logger {
    private static final Logger logger;

    static {
        logger = new Logger();
    }

    private Logger() {
    }

    public static Logger getLogger() {
        return logger;
    }

    public void log (String message) {
        System.out.println(message);
    }
}
